# GraphQL

This project uses [Apollo Kotlin](https://www.apollographql.com/docs/kotlin/)
to connect with GraphQL.

Add `.graphql` files to generate Kotlin for invoking GraphQL operations (queries, mutations, and subscriptions),
operation inputs, and schema types.

## Recommended IDE plugins

The following plugins introduce syntax highlighting, and make it easier to move back and forth between GraphQL schema
definitions:

- [GraphQL](https://plugins.jetbrains.com/plugin/8097-graphql)
  - Developer: `JetBrains s.r.o.`
  - Recommended version: _`233.13135.65` or later_

## Update the schema

1. Run the following commands to download the latest GraphQL schema from the GitLab API:

   ```shell
   ./gradlew downloadGitLabApolloSchemaFromIntrospection
   ```

1. Commit the updates to `schema.graphqls`.

### Test unreleased schema changes

1. Export `GITLAB_GRAPHQL_ENDPOINT`, pointing at `/api/graphql`, for your GDK or GitLab instance:

   ```shell
   export GITLAB_GRAPHQL_ENDPOINT=http://gdk.local/api/graphql
   ```

1. Use the `downloadApolloSchema` Gradle task to update `schema.graphqls`:

   ```shell
   ./gradlew downloadApolloSchema --endpoint=${GITLAB_GRAPHQL_ENDPOINT:-"https://gitlab.com/api/graphql"} --schema=./src/main/graphql/schema.graphqls
   ```

1. Reference the GitLab schema change merge request in any commit messages.
1. Mark any merge requests opened against this issue as blocked by the schema changes.

## Define operations

1. Visit `/-/graphql-explorer` on your GitLab instance.
1. Test your queries or mutations in [GraphiQL](https://gitlab.com/-/graphql-explorer).
1. Use the _Prettify_ button to format your operation.
1. Create a new `.graphql` file under the  `src/main/graphql` directory.
1. [Generate the sources](#generate-required-sources).

## Generate required sources

The `generateGitLabApolloSources` Gradle task generates Kotlin sources for any operations defined in
`.graphql` files under the `src/main/graphql` directory.

Usually, Gradle generates sources just-in-time. You may see some missing fields or types
until you regenerate sources. In these situations, make sure that the generated sources are up-to-date:

1. After updating the `schema.graphqls`.
1. When [defining operations](#define-operations) to new `.graphql` files with one or more operations.
1. When updating operations in existing `.graphql` files.
1. If you see any errors finding classes defined under the generated `com.gitlab.plugin.graphql` package.

To generate the required sources, run this command:

```shell
./gradlew generateGitLabApolloSources
```

### Define custom scalars

:warning: You must define a class mapping and a class adapter in the `apollo` plugin configuration inside of
`build.gradle.kts` when using [custom scalars](https://www.apollographql.com/docs/kotlin/essentials/custom-scalars/).

Define a new Kotlin class for your custom scalar inside the `com.gitlab.plugin.graphql.scalars` package with a naming
matching the GraphQL scalar.

Use the `mapGitLabScalar` helper function to register your class adapter:

```kotlin
// build.gradle.kts
apollo {
  service("GitLab") {
    // ok
    mapScalar("AiModelID", "com.gitlab.plugin.graphql.scalars.AiModelID", "com.gitlab.plugin.graphql.scalars.AiModelIDAdapter")

    // better
    mapGitLabScalar("AiModelID")
  }
}
```

After you have registered a class adapter using the `generateGitLabApolloSources` Gradle task will generate the adapter
appropriately.

### Remember GitLab version compatability

Be mindful of self-managed GitLab instances when writing your GraphQL operations. The code generation Gradle tasks
generate Kotlin classes for operations, inputs, and schema types. If your query includes fields that do not exist on
supported self-managed GitLab versions, you should confirm:

1. Use the `@optional` directive to support `null` type on required GraphQL fields.
1. Create test coverage against the operation which:
   1. Validate consuming the JSON response including the new fields behaves as expected.
   1. Validate consuming the JSON response from a previous GitLab version without the fields present default to `null`.
