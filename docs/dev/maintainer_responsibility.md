# Maintainer responsibility

Maintainers of the GitLab Duo Plugin for JetBrains are responsible for:

1. Reviewing and merging Merge Requests for the project.
1. Joining the `#f_jetbrains_plugin` Slack channel, to stay up-to-date with the project.

## Code Review

As a Maintainer, you will be pinged and assigned Merge Requests to review and merge. For example:

```plaintext
Hey @ali-gitlab! Can you please review and merge this MR? Thanks!

/assign_reviewer @ali-gitlab
```

When reviewing a Merge Request, please use your best discretion to ensure that a Merge Request meets our functional
and internal quality criteria. Some questions to ask yourself:

### Reviewing the functionality

1. Is the user-facing change something we actually want to do? You can
   check this by viewing issues referenced in the Merge Request description.
1. Does the user-facing change work as expected? Are there any edge cases
   that are accidentally or intentionally over-looked?
1. Does the pre-existing functionality still work as expected?
1. Does the Merge Request pass a reasonable smoke test?

As a Maintainer, you do not have to verify everything yourself. You may choose to delegate to the contributor.

### Reviewing the maintainability

1. Are any linting rules explicitly disabled? Why?
1. Is there a simpler approach? Why was this approach not pursued?
1. Are there any parts that are hard to follow?
1. Does this change hurt any pre-existing cohesion and responsibilities?
1. Does this change add any undesirable coupling?

## Merging

When a Merge Request is ready to merge:

1. Approve the Merge Request.
1. Ensure that the last pipeline was started no more than 24 hours ago. If the pipeline is older, start a new one.
1. Click **Set to auto-merge**, which should enqueue the Merge Request to be merged after the pipeline succeeds.

## References

- [Code Review Values](https://about.gitlab.com/handbook/engineering/workflow/reviewer-values/) for how we balance priorities and
  communication during code review.
- [Project members page](https://gitlab.com/gitlab-org/gitlab-jetbrains-plugin/-/project_members?with_inherited_permissions=exclude&sort=access_level_desc) to find the current list of active maintainers.
