const WINDOW_EVENT_TYPE = "message";
const INTELLIJ_NOT_FOUND_ERROR = "IntelliJ webview interface not found on window object";

class JetBrainsWebView {
  #intellij;
  #messageListeners = [];

  constructor(intellij) {
    if (!intellij) {
      throw new Error("intellij interface must not be null")
    }

    this.#intellij = intellij
  }

  postMessage = (message) => {
    this.#intellij?.postMessage(message);
  }

  addMessageListener = (listener) => {
    if (typeof listener !== 'function') {
      console.warn('Listener must be a function');
      return;
    }

    this.#messageListeners.push(listener);
    if (this.#messageListeners.length === 1) {
      window.addEventListener(WINDOW_EVENT_TYPE, this.#handleWindowEvent);
    }
  }

  removeMessageListener = (listener) => {
    const index = this.#messageListeners.indexOf(listener);
    if (index > -1) {
      this.#messageListeners.splice(index, 1);
    }
    if (this.#messageListeners.length === 0) {
      window.removeEventListener(WINDOW_EVENT_TYPE, this.#handleWindowEvent);
    }
  }

  #handleWindowEvent = (event) => {
    if (event.detail) {
      for (const listener of this.#messageListeners) {
        listener(event.detail);
      }
    }
  }
}

const acquireIntellijApi = () => {
  return new Promise((resolve, reject) => {
    const checkIntervalMillis = 100
    const timeoutMillis = 5000
    let elapsedTime = 0

    const checkAvailability = () => {
      if (window.intellij) {
        resolve(window.intellij)
      } else if (elapsedTime >= timeoutMillis) {
        reject(new Error(INTELLIJ_NOT_FOUND_ERROR))
      } else {
        elapsedTime += checkIntervalMillis
        setTimeout(checkAvailability, checkIntervalMillis)
      }
    }

    checkAvailability()
  })
}

export async function getJetBrainsWebViewApi() {
  const intellij = await acquireIntellijApi()
  return new JetBrainsWebView(intellij)
}
