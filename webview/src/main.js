import Vue from 'vue';
import App from './App.vue';

Vue.config.productionTip = false;

new Vue({
  render: createElement => createElement(App),
}).$mount('#app');

window.addEventListener('GitLabDuoThemeChanged', (event) => {
  document.documentElement.setAttribute("data-theme", event.detail.themeName);
})
