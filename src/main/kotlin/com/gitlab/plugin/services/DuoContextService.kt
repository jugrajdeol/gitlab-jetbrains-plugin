package com.gitlab.plugin.services

import com.gitlab.plugin.authentication.DuoPersistentSettings
import com.gitlab.plugin.ui.IconStatusModifier
import com.gitlab.plugin.util.GitLabUtil
import com.gitlab.plugin.util.TokenUtil

/**
 * Duo context service
 *
 * Includes everything that should be globally available or has no dependency on the active project
 *
 * @constructor Singleton
 */
class DuoContextService private constructor() {
  val userAgent by lazy { GitLabUtil.userAgent }

  val duoSettings by lazy { DuoPersistentSettings.getInstance() }

  val iconStatusModifier by lazy { IconStatusModifier() }

  /**
   * Return whether Duo is configured with required credentials
   *
   * @return whether duo is configured
   */
  fun isDuoConfigured(): Boolean = TokenUtil.getToken()?.isNotEmpty() == true

  /**
   * Return whether Duo is enabled
   *
   * @return whether duo is enabled
   */
  fun isDuoEnabled(): Boolean = duoSettings.enabled

  companion object {
    val instance: DuoContextService by lazy {
      DuoContextService()
    }
  }
}
