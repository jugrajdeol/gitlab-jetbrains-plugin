package com.gitlab.plugin.services

import com.gitlab.plugin.GitLabBundle
import com.gitlab.plugin.api.duo.DuoApi
import com.gitlab.plugin.ui.Notification
import com.gitlab.plugin.ui.NotificationAction
import com.intellij.openapi.diagnostic.logger
import kotlinx.coroutines.runBlocking
import java.lang.module.ModuleDescriptor.Version
import kotlin.time.Duration.Companion.hours
import kotlin.time.Duration.Companion.milliseconds

class GitLabServerService(private val api: DuoApi) {
  companion object {
    private const val BELOW_MINIMUM_GITLAB_VERSION = "16.7.9999"
    private const val GITLAB_UPDATE_DOCS_URL = "https://docs.gitlab.com/ee/update/"

    fun versionUnsupportedNotification() = Notification(
      GitLabBundle.message("notification.title.gitlab-duo"),
      GitLabBundle.message("code-suggestions.gitlab-version-unsupported"),
      listOf(
        NotificationAction.link(
          GitLabBundle.message("notification-action.link.upgrade-gitlab"),
          GITLAB_UPDATE_DOCS_URL
        )
      )
    )
  }

  private var server = Server(Version.parse("0.0.0"), lastUpdated = 0)
  private val logger = logger<GitLabServerService>()

  fun get(): Server {
    if (isExpired()) runBlocking { update() }

    return server
  }

  private fun isExpired() = (System.currentTimeMillis() - server.lastUpdated).milliseconds > 2.hours

  private suspend fun update() {
    logger.info("Updating GitLab server version")

    val metadataResponse = api.metadata() ?: return logger.info("Failed to load GitLab server version")

    server = Server(Version.parse(metadataResponse.version), lastUpdated = System.currentTimeMillis())
  }

  data class Server(val version: Version, val lastUpdated: Long) {
    val isVersionSupported: Boolean
      get() = version > Version.parse(BELOW_MINIMUM_GITLAB_VERSION)

    val isVersionUpgradeRequired: Boolean
      get() = !isVersionEmpty && !isVersionSupported

    private val isVersionEmpty: Boolean
      get() = version == Version.parse("0.0.0")
  }
}
