package com.gitlab.plugin.services

import com.gitlab.plugin.codesuggestions.telemetry.SnowplowDestination
import com.intellij.openapi.Disposable
import com.intellij.openapi.components.Service
import com.intellij.openapi.components.service

@Service(Service.Level.APP)
class GitLabApplicationService : Disposable {
  companion object {
    @JvmStatic
    fun getInstance(): GitLabApplicationService = service()
  }

  val snowplowTracker by lazy {
    with(SnowplowDestination) {
      // Pass http://localhost:9090 for collectorUrl and 1 for batchSize when testing locally with Snowplow micro
      createTracker(COLLECTOR_URL, EMITTER_BATCH_SIZE)
    }
  }

  override fun dispose() {
    snowplowTracker.close()
  }
}
