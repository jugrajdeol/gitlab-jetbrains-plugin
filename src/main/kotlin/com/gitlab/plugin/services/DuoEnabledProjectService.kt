package com.gitlab.plugin.services

import com.gitlab.plugin.api.duo.GraphQLApi
import com.gitlab.plugin.api.duo.PatProvider
import com.gitlab.plugin.api.graphql.ApolloClientFactory
import com.gitlab.plugin.chat.exceptions.GitLabGraphQLResponseException
import com.intellij.openapi.components.Service
import com.intellij.openapi.components.service
import com.intellij.openapi.diagnostic.logger
import com.intellij.openapi.project.Project

@Service(Service.Level.PROJECT)
class DuoEnabledProjectService(project: Project) {
  private val logger = logger<DuoEnabledProjectService>()
  private val apolloClientFactory = ApolloClientFactory(tokenProvider = PatProvider())
  private val graphQLApi = GraphQLApi(apolloClientFactory)
  private val gitLabProjectService = project.service<GitLabProjectService>()

  suspend fun isDuoEnabledForProject(): Boolean {
    var isDuoEnabled = true
    val projectPath = gitLabProjectService.getCurrentProjectPath()

    try {
      if (projectPath != null) {
        val graphQLProject = graphQLApi.getProject(projectPath)
        if (graphQLProject?.duoFeaturesEnabled != null) {
          isDuoEnabled = graphQLProject.duoFeaturesEnabled
        } else {
          logger.info("The project retrieved from GraphQL is null.")
        }
      }
    } catch (exception: GitLabGraphQLResponseException) {
      logger.info("GraphQL API call returned an error: ${exception.message}")
    }
    return isDuoEnabled
  }
}
