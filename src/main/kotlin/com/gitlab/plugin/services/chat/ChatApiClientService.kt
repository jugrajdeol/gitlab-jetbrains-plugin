package com.gitlab.plugin.services.chat

import com.gitlab.plugin.api.duo.GraphQLApi
import com.gitlab.plugin.api.duo.PatProvider
import com.gitlab.plugin.api.graphql.ApolloClientFactory
import com.gitlab.plugin.chat.api.abstraction.ChatApiClient
import com.gitlab.plugin.chat.api.model.AiActionResponse
import com.gitlab.plugin.chat.api.model.AiMessage
import com.gitlab.plugin.chat.model.ChatRecordContext
import io.ktor.util.*
import kotlinx.coroutines.delay
import kotlinx.datetime.Clock
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toLocalDateTime
import java.util.*

private const val AI_RESPONSE_DELAY = 1000L

internal class ChatApiClientService : ChatApiClient {
  private val requestIdsBySubscriptionIds = HashMap<String, String>()
  private val tokenProvider = PatProvider()
  private val apolloClientFactory = ApolloClientFactory(tokenProvider = tokenProvider)
  private val graphQLApi = GraphQLApi(apolloClientFactory)

  override suspend fun processNewUserPrompt(
    subscriptionId: String,
    question: String,
    context: ChatRecordContext?
  ): AiActionResponse? {
    val action = graphQLApi.chatMutation(question, subscriptionId, context)

    return action?.let {
      requestIdsBySubscriptionIds[subscriptionId] = action.requestId
      AiActionResponse(it)
    }
  }

  override suspend fun subscribeToUpdates(
    subscriptionId: String,
    onMessageReceived: suspend (message: AiMessage) -> Unit
  ) {
    val requestId = requestIdsBySubscriptionIds[subscriptionId]
      ?: throw RequestIdNotFoundException(
        "Could not find requestId for subscriptionId: $subscriptionId"
      )

    var done = false
    while (!done) {
      val resp = graphQLApi.chatQuery(requestId)
      if (resp.nodes == null) {
        return
      }
      if (resp.nodes.size <= 1) {
        delay(AI_RESPONSE_DELAY)
        continue
      }

      for (node in resp.nodes) {
        val response =
          AiMessage(
            requestId = requestId,
            role = node?.role.toString().toLowerCasePreservingASCIIRules(),
            content = node?.content.orEmpty(),
            contentHtml = node?.contentHtml.orEmpty(),
            timestamp = Clock.System.now().toLocalDateTime(TimeZone.UTC).toString()
          )

        onMessageReceived(response)
      }
      done = true
    }
  }
}

class RequestIdNotFoundException(message: String) : Exception(message)
