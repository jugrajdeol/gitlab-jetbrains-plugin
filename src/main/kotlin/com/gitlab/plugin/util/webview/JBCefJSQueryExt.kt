package com.gitlab.plugin.util.webview

import com.intellij.ui.jcef.JBCefJSQuery

fun JBCefJSQuery.addHandler(handler: (String) -> Unit): JBCefJSQuery = apply {
  addHandler {
    handler(it)
    null
  }
}
