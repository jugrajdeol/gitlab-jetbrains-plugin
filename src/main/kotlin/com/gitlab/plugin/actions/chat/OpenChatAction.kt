package com.gitlab.plugin.actions.chat

import com.gitlab.plugin.BuildConfig
import com.gitlab.plugin.services.chat.ChatService
import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.components.service

class OpenChatAction : AnAction() {
  override fun actionPerformed(event: AnActionEvent) {
    event.project?.service<ChatService>()?.showChatWindow()
  }

  override fun update(event: AnActionEvent) {
    event.presentation.isVisible = BuildConfig.DUO_CHAT_ENABLED
  }
}
