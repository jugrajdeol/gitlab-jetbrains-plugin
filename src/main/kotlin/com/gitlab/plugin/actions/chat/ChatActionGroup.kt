package com.gitlab.plugin.actions.chat

import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.DefaultActionGroup

class ChatActionGroup : DefaultActionGroup() {
  override fun update(e: AnActionEvent) {
    e.presentation.isHideGroupIfEmpty = true
  }
}
