package com.gitlab.plugin.graphql.scalars

/**
 * A base class for serializing different scalars to/from JSON.
 */
open class GitLabScalar(val value: String)
