package com.gitlab.plugin.api.graphql

import com.apollographql.apollo3.ApolloClient
import com.apollographql.apollo3.network.http.DefaultHttpEngine
import com.apollographql.apollo3.network.ws.WebSocketNetworkTransport
import com.gitlab.plugin.api.ProxyManager
import com.gitlab.plugin.api.asProxyInfo
import com.gitlab.plugin.api.duo.DuoClient
import com.gitlab.plugin.api.graphql.apollo.actioncable.ActionCableWebSocketEngine
import com.gitlab.plugin.api.graphql.apollo.actioncable.ActionCableWsProtocolFactory
import com.gitlab.plugin.util.GitLabUtil.GITLAB_DEFAULT_URL
import com.intellij.collaboration.util.resolveRelative
import com.intellij.util.net.HttpConfigurable
import io.ktor.http.HttpHeaders.Authorization
import io.ktor.http.HttpHeaders.Origin
import okhttp3.OkHttpClient
import java.net.URI

class ApolloClientFactory(
  private val tokenProvider: DuoClient.TokenProvider,
  gitlabHost: String = GITLAB_DEFAULT_URL,
) {
  private val instanceUri = URI(gitlabHost)
  private val serverUrl: String = instanceUri
    .resolveRelative("/api/graphql")
    .toString()
  private val subscriptionUrl: String = instanceUri
    .resolveRelative("/-/cable")
    .toString()
    .replace("https://", "wss://")
    .replace("http://", "ws://")
  private val okHttpClient: OkHttpClient by lazy {
    OkHttpClient.Builder().apply {
      followRedirects(true)

      val proxyInfo = HttpConfigurable.getInstance().asProxyInfo()
      proxyInfo?.let {
        ProxyManager(it) { url -> HttpConfigurable.getInstance().isHttpProxyEnabledForUrl(url) }.let { proxyManager ->
          proxySelector(proxyManager)
          proxyAuthenticator(proxyManager)
        }
      }
    }.build()
  }

  fun create(): ApolloClient = ApolloClient.Builder()
    .addHttpHeader(Authorization, bearerToken())
    .httpEngine(DefaultHttpEngine(okHttpClient))
    .serverUrl(serverUrl)
    .subscriptionNetworkTransport(
      WebSocketNetworkTransport.Builder()
        .serverUrl(subscriptionUrl)
        .addHeader(Origin, instanceUri.toString())
        .addHeader(Authorization, bearerToken())
        .webSocketEngine(ActionCableWebSocketEngine(okHttpClient))
        .protocol(ActionCableWsProtocolFactory())
        .build()
    )
    .build()

  private fun bearerToken() = "Bearer ${tokenProvider.token()}"
}
