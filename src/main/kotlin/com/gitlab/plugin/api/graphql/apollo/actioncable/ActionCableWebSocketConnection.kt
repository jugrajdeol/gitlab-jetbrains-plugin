package com.gitlab.plugin.api.graphql.apollo.actioncable

import com.apollographql.apollo3.network.ws.CLOSE_NORMAL
import com.apollographql.apollo3.network.ws.WebSocketConnection
import com.intellij.openapi.diagnostic.Logger
import com.intellij.openapi.diagnostic.logger
import kotlinx.coroutines.channels.Channel
import okhttp3.WebSocket
import okio.ByteString

class ActionCableWebSocketConnection(
  private val messageChannel: Channel<String>,
  private val webSocket: WebSocket,
) : WebSocketConnection {
  private val logger: Logger = logger<ActionCableWebSocketConnection>()

  override fun close() {
    webSocket.close(CLOSE_NORMAL, null)
  }

  override suspend fun receive() = messageChannel.receive()

  override fun send(data: ByteString) {
    if (logger.isDebugEnabled) logger.debug("sendBytes: ${data.utf8()}")
    if (!webSocket.send(data)) {
      // The websocket is full or closed
      messageChannel.close()
    }
  }

  override fun send(string: String) {
    logger.trace("sendText: $string")
    if (!webSocket.send(string)) {
      // The websocket is full or closed
      messageChannel.close()
    }
  }
}
