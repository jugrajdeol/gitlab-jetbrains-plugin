package com.gitlab.plugin.api.graphql.apollo.actioncable

import com.apollographql.apollo3.exception.ApolloWebSocketClosedException
import com.intellij.openapi.diagnostic.Logger
import com.intellij.openapi.diagnostic.logger
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.trySendBlocking
import okhttp3.Response
import okhttp3.WebSocket
import okhttp3.WebSocketListener
import okio.ByteString

class ActionCableWebSocketListener(
  private val messageChannel: Channel<String>,
  private val webSocketOpenResult: CompletableDeferred<Unit>
) : WebSocketListener() {
  private val logger: Logger = logger<ActionCableWebSocketListener>()

  override fun onOpen(webSocket: WebSocket, response: Response) {
    logger.debug("onOpen(webSocket: $webSocket, response: $response")
    webSocketOpenResult.complete(Unit)
  }

  override fun onMessage(webSocket: WebSocket, text: String) {
    logger.debug("onMessage(webSocket: $webSocket, text: $text")
    messageChannel.trySendBlocking(text)
  }

  override fun onMessage(webSocket: WebSocket, bytes: ByteString) {
    logger.debug("onMessage(webSocket: $webSocket, bytes: $bytes")
    messageChannel.trySendBlocking(bytes.utf8())
  }

  override fun onFailure(webSocket: WebSocket, t: Throwable, response: Response?) {
    logger.debug("onFailure(webSocket: $webSocket, t: $t, response: $response)")
    webSocketOpenResult.complete(Unit)
    messageChannel.close(t)
  }

  override fun onClosing(webSocket: WebSocket, code: Int, reason: String) {
    logger.debug("onClosing: (code: $code, reason: $reason)")
    webSocketOpenResult.complete(Unit)

    messageChannel.close(ApolloWebSocketClosedException(code, reason))
  }

  override fun onClosed(webSocket: WebSocket, code: Int, reason: String) {
    logger.debug("onClosed: $code - $reason")
    messageChannel.close()
  }
}
