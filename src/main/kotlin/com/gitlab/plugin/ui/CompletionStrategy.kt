package com.gitlab.plugin.ui

import com.gitlab.plugin.api.duo.DuoApi
import com.gitlab.plugin.api.duo.requests.Completion
import com.gitlab.plugin.services.GitLabServerService
import com.intellij.openapi.diagnostic.logger

class CompletionStrategy(private val duoApi: DuoApi, private val serverService: GitLabServerService) {
  private val logger = logger<CompletionStrategy>()

  suspend fun generateCompletions(payload: Completion.Payload): Completion.Response? =
    if (serverService.get().isVersionSupported) {
      duoApi.completions(payload)
    } else {
      logger.info("Code Suggestions are not supported in this GitLab version")
      null
    }
}
