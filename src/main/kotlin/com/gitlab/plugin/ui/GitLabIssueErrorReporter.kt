package com.gitlab.plugin.ui

import com.gitlab.plugin.GitLabBundle
import com.gitlab.plugin.util.GitLabUtil
import com.intellij.openapi.application.ApplicationInfo
import com.intellij.openapi.diagnostic.ErrorReportSubmitter
import com.intellij.openapi.diagnostic.IdeaLoggingEvent
import com.intellij.openapi.diagnostic.SubmittedReportInfo
import com.intellij.openapi.util.SystemInfo
import com.intellij.util.Consumer
import io.ktor.http.*
import java.awt.Component

// default character limit set by cloudflare - 6000 (leaving space for other headers including cookie)
const val HEADER_CHARACTER_LIMIT = 10_000

// Leave space for 25% more characters from URL encoding
const val STACKTRACE_CHARACTER_LIMIT = HEADER_CHARACTER_LIMIT.div(1.25).toInt()

class GitLabIssueErrorReporter : ErrorReportSubmitter() {
  override fun getReportActionText() = GitLabBundle.message("error-reporter.action-text")

  override fun submit(
    events: Array<out IdeaLoggingEvent>,
    additionalInfo: String?,
    parentComponent: Component,
    consumer: Consumer<in SubmittedReportInfo>
  ): Boolean {
    events.forEach { event ->
      val issueUrl = gitlabIssueUrl(event, additionalInfo.orEmpty())
      GitLabUtil.browseUrl(issueUrl)
    }

    consumer.consume(SubmittedReportInfo(SubmittedReportInfo.SubmissionStatus.NEW_ISSUE))

    return true
  }

  private fun gitlabIssueUrl(event: IdeaLoggingEvent, additionalInfo: String): String {
    val ideInfo = ApplicationInfo.getInstance().let { appInfo ->
      appInfo.fullApplicationName + "; Build: " + appInfo.build.asString()
    }

    val jdkInfo = with(System.getProperties()) {
      getProperty("java.version", "unknown") +
        "; VM: " + getProperty("java.vm.name", "unknown") +
        "; Vendor: " + getProperty("java.vendor", "unknown")
    }

    val osInfo = "${SystemInfo.getOsNameAndVersion()}; Arch: ${SystemInfo.OS_ARCH}"

    val aboveStackTrace = """
      |## Debug information
      |
      |<details><summary>Stack trace</summary>
      |
      |```
      |
    """.trimMargin().encodeURLParameter()

    val belowStackTrace = """
      |
      |```
      |
      |</details>
      |
      |### Environment
      |
      |- Plugin version: ${GitLabBundle.plugin()?.version ?: "unknown"}
      |- IDE: $ideInfo
      |- JDK: $jdkInfo
      |- OS: $osInfo
      |
      |### Additional information
      |
      |$additionalInfo
    """.trimMargin().encodeURLParameter()

    val issueURL = "https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/new" +
      "?issuable_template=Bug&issue[description]="

    val stackTrace = event.throwableText.take(
      STACKTRACE_CHARACTER_LIMIT - aboveStackTrace.length - belowStackTrace.length - issueURL.length
    ).encodeURLParameter()

    return issueURL + aboveStackTrace + stackTrace + belowStackTrace
  }
}
