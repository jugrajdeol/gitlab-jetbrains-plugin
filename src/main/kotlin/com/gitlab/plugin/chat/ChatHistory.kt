package com.gitlab.plugin.chat

import com.gitlab.plugin.chat.model.ChatRecord

class ChatHistory(
  initialRecords: List<ChatRecord> = emptyList()
) {
  private val _records = mutableListOf<ChatRecord>()
  val records get() = _records.toList()

  init {
    _records.addAll(initialRecords)
  }

  fun addRecord(
    record: ChatRecord
  ) {
    _records.add(record)
  }

  fun findViaRequestId(
    requestId: String,
    role: ChatRecord.Role
  ): ChatRecord? =
    _records.find { r -> r.requestId == requestId && r.role == role }
}
