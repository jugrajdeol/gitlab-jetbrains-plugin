package com.gitlab.plugin.chat.api.model

data class AiActionResponse(
  val aiAction: AiAction
)
