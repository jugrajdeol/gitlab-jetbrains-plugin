package com.gitlab.plugin

import com.gitlab.plugin.util.Status

class GitLabSettingsChangeEvent(val status: Status = Status.DISABLED)
