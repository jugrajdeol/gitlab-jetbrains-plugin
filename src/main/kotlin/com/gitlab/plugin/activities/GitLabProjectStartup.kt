package com.gitlab.plugin.activities

import com.gitlab.plugin.GitLabBundle
import com.gitlab.plugin.codesuggestions.license.LicenseStatus
import com.gitlab.plugin.services.DuoEnabledProjectService
import com.gitlab.plugin.services.GitLabServerService
import com.gitlab.plugin.services.ProjectContextService
import com.gitlab.plugin.ui.GitLabNotificationManager
import com.gitlab.plugin.ui.Notification
import com.gitlab.plugin.ui.NotificationAction
import com.gitlab.plugin.util.TokenUtil
import com.gitlab.plugin.util.gitlabStatusDisabled
import com.gitlab.plugin.util.gitlabStatusVersionUnsupported
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import com.intellij.openapi.startup.ProjectActivity

internal class GitLabProjectStartup : ProjectActivity {
  override suspend fun execute(project: Project) {
    val duoEnabledProjectService = project.service<DuoEnabledProjectService>()
    val context = ProjectContextService.init(project)
    val isDuoEnabled = duoEnabledProjectService.isDuoEnabledForProject()

    context.codeSuggestionsLicense.refresh()
    val token = TokenUtil.getToken()

    val notification = when {
      !isDuoEnabled -> {
        gitlabStatusDisabled()
        val title = GitLabBundle.message("notification.title.gitlab-duo")
        val message = GitLabBundle.message("notification.duo-disabled")
        Notification(title, message)
      }
      !context.codeSuggestionsLicense.isLicensed -> {
        LicenseStatus.licenseMissingNotification()
      }

      context.serverServiceWithoutCallbacks.get().isVersionUpgradeRequired -> {
        gitlabStatusVersionUnsupported()
        GitLabServerService.versionUnsupportedNotification()
      }

      !token.isNullOrEmpty() -> {
        Notification(
          "Get started with GitLab Duo Code Suggestions.",
          "GitLab Duo Code Suggestions is ready to use with this project."
        )
      }

      else -> {
        Notification(
          "Get started with GitLab Duo Code Suggestions.",
          "You need to configure your GitLab credentials first.",
          listOf(NotificationAction.settings(project))
        )
      }
    }

    GitLabNotificationManager().sendNotification(notification)
  }
}
