package com.gitlab.plugin.codesuggestions.license

import com.gitlab.plugin.GitLabBundle
import com.gitlab.plugin.api.GitLabResponseException
import com.gitlab.plugin.api.GitLabUnauthorizedException
import com.gitlab.plugin.api.duo.DuoApi
import com.gitlab.plugin.api.duo.ExceptionHandler
import com.gitlab.plugin.api.pat.PatApi
import com.gitlab.plugin.api.pat.PatInfo
import com.gitlab.plugin.ui.Notification
import com.intellij.openapi.diagnostic.logger
import io.ktor.client.plugins.*
import io.ktor.http.*

class LicenseStatus(private val patApi: PatApi, private val duoApi: DuoApi, private val onLicenseChanged: () -> Unit) {
  companion object {
    val exceptionHandler = ExceptionHandler {
      if (it is GitLabUnauthorizedException || it is GitLabResponseException) throw it
    }

    fun licenseMissingNotification() = Notification(
      GitLabBundle.message("notification.title.gitlab-duo"),
      GitLabBundle.message("code-suggestions.not-licensed")
    )
  }

  var isLicensed: Boolean = true
  private val logger = logger<LicenseStatus>()

  suspend fun refresh() {
    logger.info("Checking Code Suggestions license")

    if (!isPatValid()) return logger.info("Code Suggestions license check failed due to invalid PAT")

    try {
      if (duoApi.verify()) {
        licensed()
        logger.info("License check using verify request succeeded")
      }
    } catch (e: GitLabUnauthorizedException) {
      notLicensed()
      logger.info("License check using verify threw GitLabUnauthorizedException: ${e.message}")
    } catch (e: GitLabResponseException) {
      if (e.response.status == HttpStatusCode.NotFound) {
        notLicensed()
        logger.info("License check using verify threw GitLabResponseException with 404")
      }
    }
  }

  private fun notLicensed() {
    logger.info("Code Suggestions license not found")

    isLicensed = false
    onLicenseChanged()
  }

  private fun licensed() {
    logger.info("Code Suggestions license found")

    isLicensed = true
    onLicenseChanged()
  }

  private suspend fun isPatValid(): Boolean = try {
    patApi.patInfo().let {
      it.active && it.scopes.contains(PatInfo.AI_FEATURES_SCOPE)
    }
  } catch (e: ResponseException) {
    logger.info("Failed to check PAT validity due to ${e.message}")
    false
  }
}
