package com.gitlab.plugin.codesuggestions.telemetry

import com.intellij.codeInsight.inline.completion.InlineCompletion
import com.intellij.openapi.editor.event.EditorFactoryEvent
import com.intellij.openapi.editor.event.EditorFactoryListener

internal class EditorListener : EditorFactoryListener {
  private val listener = InlineCompletionEventListener()

  override fun editorCreated(event: EditorFactoryEvent) {
    InlineCompletion.getHandlerOrNull(event.editor)?.addEventListener(listener)
  }
}
