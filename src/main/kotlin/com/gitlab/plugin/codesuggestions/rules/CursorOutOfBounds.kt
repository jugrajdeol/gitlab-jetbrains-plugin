package com.gitlab.plugin.codesuggestions.rules

import com.intellij.codeInsight.inline.completion.InlineCompletionRequest
import com.intellij.openapi.diagnostic.Logger

/**
Suggestions should not be provided when the cursor is out of bounds. This is because getting the context
(aboveCursor, belowCursor) would result in IndexOutOfBoundsException.

For more details, see https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/32#note_1476601708.
 */
class CursorOutOfBounds(logger: Logger = Logger.getInstance(CursorOutOfBounds::class.java)) : BaseSkipRule(logger) {
  private val logPrefix = "cursor is out of bounds"

  override fun shouldSkipSuggestion(request: InlineCompletionRequest): Boolean {
    val document = request.document
    val currentPosition = request.endOffset

    return when {
      currentPosition < 0 -> {
        log("cursor position ($currentPosition) is negative")
        true
      }

      currentPosition > document.textLength -> {
        log("cursor position ($currentPosition) is greater than the size of the document (${document.textLength})")
        true
      }

      else -> false
    }
  }

  override fun log(message: String) =
    super.log("$logPrefix: $message")
}
