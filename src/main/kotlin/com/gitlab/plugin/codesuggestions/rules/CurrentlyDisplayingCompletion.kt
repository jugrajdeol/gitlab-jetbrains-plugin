package com.gitlab.plugin.codesuggestions.rules

import com.intellij.codeInsight.inline.completion.InlineCompletionRequest
import com.intellij.codeInsight.inline.completion.session.InlineCompletionContext
import com.intellij.openapi.diagnostic.Logger

/**
Suggestions should not be provided when an inline completion is currently displaying.
 */
class CurrentlyDisplayingCompletion(logger: Logger = Logger.getInstance(CurrentlyDisplayingCompletion::class.java)) :
  BaseSkipRule(logger) {
  override fun shouldSkipSuggestion(request: InlineCompletionRequest): Boolean {
    if (InlineCompletionContext.getOrNull(request.editor)?.isCurrentlyDisplaying() != true) return false

    log("completion is currently displaying")
    return true
  }
}
