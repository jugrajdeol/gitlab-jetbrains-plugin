package com.gitlab.plugin.services.chat

import com.apollographql.apollo3.ApolloClient
import com.gitlab.plugin.api.duo.GraphQLApi
import com.gitlab.plugin.api.graphql.ApolloClientFactory
import com.gitlab.plugin.chat.api.model.AiAction
import com.gitlab.plugin.chat.api.model.AiMessage
import com.gitlab.plugin.chat.model.ChatRecordContext
import com.gitlab.plugin.chat.model.ChatRecordFileContext
import com.gitlab.plugin.graphql.ChatQuery
import com.gitlab.plugin.graphql.type.AiMessageRole
import com.gitlab.plugin.graphql.type.AiMessageType
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.nulls.shouldNotBeNull
import io.kotest.matchers.shouldBe
import io.mockk.*

class ChatApiClientServiceTest : DescribeSpec({
  beforeContainer {
    mockkConstructor(GraphQLApi::class)
    mockkConstructor(ApolloClientFactory::class)
  }

  afterContainer {
    unmockkAll()
  }

  describe("ChatApiClientService") {
    val mockApolloClient = mockk<ApolloClient>()

    beforeEach {
      clearMocks(mockApolloClient)
      every { anyConstructed<ApolloClientFactory>().create() } returns mockApolloClient
    }

    describe("processNewUserPrompt") {

      it("calls the chat mutation graphql api") {

        val ctx = ChatRecordContext(
          currentFile = ChatRecordFileContext(fileName = "file.txt", selectedText = "some text")
        )
        coEvery {
          anyConstructed<GraphQLApi>().chatMutation("random question", "123", ctx)
        } returns AiAction("abc", emptyList())
        val service = ChatApiClientService()
        val response = service.processNewUserPrompt("123", "random question", ctx)
        response.shouldNotBeNull()
        response.aiAction.errors shouldBe emptyList()
        response.aiAction.requestId shouldBe "abc"
      }

      it("returns null when the api does not return anything") {

        coEvery {
          anyConstructed<GraphQLApi>().chatMutation("random question", "123")
        } returns null
        val service = ChatApiClientService()
        val response = service.processNewUserPrompt("123", "random question")
        response shouldBe null
      }
    }

    describe("subscribeToUpdates") {

      val subscriptionId = "123"
      val requestId = "abc"

      beforeEach {
        coEvery {
          anyConstructed<GraphQLApi>().chatMutation("random question", subscriptionId)
        } returns AiAction(requestId, emptyList())
      }

      it("calls onMessageReceived for each message sent back from the API") {
        coEvery { anyConstructed<GraphQLApi>().chatQuery(requestId) } returns ChatQuery.Messages(
          listOf(
            ChatQuery.Node(
              requestId = "xxx",
              role = AiMessageRole.USER, content = "Hello", type = AiMessageType.UNKNOWN__, contentHtml = "<p>Hello</p>"
            ),
            ChatQuery.Node(
              requestId = "xxx",
              role = AiMessageRole.ASSISTANT, content = "Hi there!", type = AiMessageType.UNKNOWN__,
              contentHtml = "<p>Hi there!</p>"
            )
          )
        )

        val service = ChatApiClientService()
        service.processNewUserPrompt(subscriptionId, "random question")

        val mockOnMessageReceived: suspend (AiMessage) -> Unit = mockk(relaxed = true)
        service.subscribeToUpdates(subscriptionId, mockOnMessageReceived)

        coVerify(exactly = 2) { mockOnMessageReceived(any()) }
      }

      it("does not call onMessageReceived if an empty result is returned") {
        coEvery { anyConstructed<GraphQLApi>().chatQuery(requestId) } returns ChatQuery.Messages(
          null
        )

        val service = ChatApiClientService()
        service.processNewUserPrompt(subscriptionId, "random question")

        val mockOnMessageReceived: suspend (AiMessage) -> Unit = mockk(relaxed = true)
        service.subscribeToUpdates(subscriptionId, mockOnMessageReceived)

        coVerify(exactly = 0) { mockOnMessageReceived(any()) }
      }
    }
  }
})
