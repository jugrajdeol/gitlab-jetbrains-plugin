package com.gitlab.plugin.api.duo.requests

import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe

class CompletionTest : DescribeSpec({
  describe("Response") {
    val model = Completion.Response.Model()

    describe("hasSuggestions") {
      context("when choices is an empty list") {
        it("returns false") {
          Completion.Response(emptyList(), model).hasSuggestions() shouldBe false
        }
      }

      context("when all choices are empty strings") {
        val choices = listOf(Completion.Response.Choice(""), Completion.Response.Choice(" "))

        it("returns false") {
          Completion.Response(choices, model).hasSuggestions() shouldBe false
        }
      }

      describe("when choices contains at least one non-empty string") {
        val choices = listOf(Completion.Response.Choice("A code suggestion"))

        it("returns true") {
          Completion.Response(choices, model).hasSuggestions() shouldBe true
        }
      }
    }

    describe("firstSuggestion") {
      val choices = listOf(Completion.Response.Choice("A code suggestion"))

      it("returns the text of the first choice") {
        Completion.Response(choices, model).firstSuggestion shouldBe "A code suggestion"
      }
    }
  }
})
