package com.gitlab.plugin.actions

import com.gitlab.plugin.actions.chat.OpenChatAction
import com.gitlab.plugin.services.chat.ChatService
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.components.service
import io.kotest.core.spec.style.DescribeSpec
import io.mockk.*

class OpenChatActionTest : DescribeSpec({
  describe("OpenChatAction") {
    afterEach { clearAllMocks() }
    afterSpec { unmockkAll() }

    val mockActionEvent = mockk<AnActionEvent>(relaxed = true)
    val mockChatService = mockk<ChatService>(relaxed = true)

    beforeEach {
      every { mockActionEvent.project?.service<ChatService>() } returns mockChatService
    }

    describe("actionPerformed") {
      it("calls showChatWindow") {
        val action = OpenChatAction()
        action.actionPerformed(mockActionEvent)

        coVerify(exactly = 1) { mockChatService.showChatWindow() }
      }
    }
  }
})
