package com.gitlab.plugin.chat

import com.gitlab.plugin.chat.api.abstraction.ChatApiClient
import com.gitlab.plugin.chat.api.model.AiAction
import com.gitlab.plugin.chat.api.model.AiActionResponse
import com.gitlab.plugin.chat.api.model.AiMessage
import com.gitlab.plugin.chat.extensions.fromSelectedEditor
import com.gitlab.plugin.chat.model.ChatRecord
import com.gitlab.plugin.chat.model.ChatRecordContext
import com.gitlab.plugin.chat.model.NewUserPromptRequest
import com.gitlab.plugin.chat.view.abstraction.ChatView
import com.gitlab.plugin.chat.view.model.AppReadyMessage
import com.gitlab.plugin.chat.view.model.ChatViewMessage
import com.gitlab.plugin.chat.view.model.NewPromptMessage
import com.intellij.openapi.project.Project
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.collections.shouldHaveSize
import io.kotest.matchers.shouldBe
import io.mockk.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import java.util.*

@OptIn(ExperimentalCoroutinesApi::class)
class ChatControllerTest : DescribeSpec({
  lateinit var controller: ChatController
  val api = mockk<ChatApiClient>(relaxed = true)
  val view = mockk<ChatView>(relaxed = true)
  val project: Project = mockk(relaxed = true)
  lateinit var chatHistory: ChatHistory
  lateinit var viewOnMessageCallback: ((message: ChatViewMessage) -> Unit)
  val testDispatcher = UnconfinedTestDispatcher()

  beforeEach {
    every {
      view.onMessage(captureLambda())
    } answers {
      viewOnMessageCallback = lambda<(message: ChatViewMessage) -> Unit>().captured
    }

    chatHistory = ChatHistory()
    controller =
      ChatController(
        api, view, chatHistory = chatHistory,
        dispatcher = testDispatcher,
        project = project,
      )
  }

  afterEach { clearAllMocks() }
  afterSpec { unmockkAll() }

  describe("init") {
    describe("chatView") {
      describe("onMessage") {
        describe("NewPromptMessage") {
          context("when prompt is a known command") {
            mockkStatic(ChatRecordContext::fromSelectedEditor)
            val context: ChatRecordContext = mockk(relaxed = true)

            beforeEach {
              coEvery { ChatRecordContext.fromSelectedEditor(project) } returns context
            }

            it("processes new user prompt with context") {
              val knownCommand = "/explain"

              runBlocking { viewOnMessageCallback(NewPromptMessage(knownCommand)) }

              coVerify(exactly = 1) {
                api.processNewUserPrompt(any(), any(), match { it == context })
              }
            }
          }

          context("when prompt is not a known command") {
            it("processes new user prompt without context") {
              val knownCommand = "/unknown"

              runBlocking { viewOnMessageCallback(NewPromptMessage(knownCommand)) }

              coVerify(exactly = 1) {
                api.processNewUserPrompt(any(), any(), null)
              }
            }
          }

          it("processes new user prompt") {
            runTest(testDispatcher) {
              // act
              val prompt = "ping"
              viewOnMessageCallback(NewPromptMessage(prompt))

              // assert
              verify(exactly = 1) { view.show() }
              verify {
                view.addRecord(
                  match {
                    it.record == chatHistory.records[0]
                  }
                )
              }

              verify {
                view.addRecord(
                  match {
                    it.record == chatHistory.records[1]
                  }
                )
              }
            }
          }
        }

        describe("appReadyMessage") {
          it("restores history to view") {
            // arrange
            val chatRecords = listOf(
              ChatRecord(
                role = ChatRecord.Role.USER,
                state = ChatRecord.State.READY,
                requestId = UUID.randomUUID().toString(),
                content = "User message"
              ),
              ChatRecord(
                role = ChatRecord.Role.ASSISTANT,
                state = ChatRecord.State.READY,
                requestId = UUID.randomUUID().toString(),
                content = "Assistant response"
              )
            )

            chatRecords.forEach { chatHistory.addRecord(it) }

            // act
            viewOnMessageCallback.invoke(AppReadyMessage)

            // assert
            // Verify each record in chat history is added to the view
            chatRecords.forEach { record ->
              verify {
                view.addRecord(
                  match {
                    it.record == record
                  }
                )
              }
            }
          }
        }
      }
    }

    describe("processNewUserPrompt") {
      describe("with GENERAL type and valid input") {
        it("add user and assistant record") {
          // arrange
          val content = "ping"
          val requestId = "00000000-00000000-00000000-00000000"

          coEvery {
            api.processNewUserPrompt(any(), any(), any())
          } returns AiActionResponse(AiAction(requestId, emptyList()))

          // act
          controller.processNewUserPrompt(NewUserPromptRequest(content))

          // assert

          // assert chat history
          chatHistory.records shouldHaveSize 2

          chatHistory.records[0].let {
            it.role shouldBe ChatRecord.Role.USER
            it.content shouldBe content
          }

          chatHistory.records[1].role shouldBe ChatRecord.Role.ASSISTANT

          val subscriptionId = chatHistory.records[0].id

          // assert view
          verify(exactly = 1) { view.show() }
          verify {
            view.addRecord(
              match {
                it.record == chatHistory.records[0]
              }
            )
          }

          verify {
            view.addRecord(
              match {
                it.record == chatHistory.records[1]
              }
            )
          }

          // assert api client
          coVerify { api.processNewUserPrompt(subscriptionId, content) }
          coVerify { api.subscribeToUpdates(subscriptionId, any()) }
        }

        it("sets up subscription and handles updates correctly") {
          // arrange
          val content = "ping"
          val requestId = UUID.randomUUID().toString()
          val aiActionResponse = AiActionResponse(AiAction(requestId, emptyList()))
          val newUserPromptRequest = NewUserPromptRequest(content)

          coEvery { api.processNewUserPrompt(any(), any(), any()) } returns aiActionResponse

          val assistantContent = "pong"
          val aiMessage =
            AiMessage(
              requestId,
              role = "assistant",
              content = assistantContent,
              contentHtml = "",
              timestamp = ""
            )

          coEvery { api.subscribeToUpdates(any(), any()) } coAnswers {
            secondArg<suspend (message: AiMessage) -> Unit>().invoke(aiMessage)
          }

          // act
          controller.processNewUserPrompt(newUserPromptRequest)

          // assert
          // Verify that subscribeToUpdates is called with correct parameters
          coVerify { api.subscribeToUpdates(any(), any()) }

          // Verify that the chat history and view are updated with the new information from the subscription
          chatHistory.records.last().let {
            it.role shouldBe ChatRecord.Role.ASSISTANT
            it.content shouldBe assistantContent

            // Verify that the view is updated with the new assistant record
            verify {
              view.updateRecord(
                match { message ->
                  message.record == it
                }
              )
            }
          }
        }
      }

      describe("with NEW_CONVERSATION type") {
        it("adds user record while not adding new assistant record") {
          // arrange
          val content = "Start New Conversation"
          val requestId = "newConvRequestId"
          val newUserPromptRequest = NewUserPromptRequest(content, ChatRecord.Type.NEW_CONVERSATION)

          coEvery {
            api.processNewUserPrompt(any(), any(), any())
          } returns AiActionResponse(AiAction(requestId, emptyList()))

          // act
          controller.processNewUserPrompt(newUserPromptRequest)

          // assert

          // Assert that a new conversation record is added to chat history
          chatHistory.records shouldHaveSize 1
          chatHistory.records[0].let {
            it.role shouldBe ChatRecord.Role.USER
            it.type shouldBe ChatRecord.Type.NEW_CONVERSATION
            it.content shouldBe content
          }

          // Assert that the view is shown and the record is added to the view
          verify(exactly = 1) { view.show() }
          verify {
            view.addRecord(
              match {
                it.record == chatHistory.records[0]
              }
            )
          }

          // Assert that no assistant record is created and no subscription to updates is made
          coVerify(exactly = 0) { api.subscribeToUpdates(any(), any()) }
        }
      }
    }
  }
})
