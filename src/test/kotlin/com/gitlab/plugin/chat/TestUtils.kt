package com.gitlab.plugin.chat

import com.gitlab.plugin.chat.model.ChatRecord
import kotlinx.datetime.LocalDateTime
import java.util.*

fun buildChatRecord(
  id: String = UUID.randomUUID().toString(),
  requestId: String = UUID.randomUUID().toString(),
  timestamp: LocalDateTime = buildLocalDateTime()
) = ChatRecord(
  id = id,
  role = ChatRecord.Role.USER,
  requestId = requestId,
  state = ChatRecord.State.READY,
  timestamp = timestamp
)

fun buildLocalDateTime() = LocalDateTime.parse("2022-01-01T00:00:00")
