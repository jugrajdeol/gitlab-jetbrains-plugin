package com.gitlab.plugin.util

import com.gitlab.plugin.GitLabSettingsChangeEvent
import com.gitlab.plugin.GitLabSettingsListener
import com.gitlab.plugin.services.ProjectContextService
import com.intellij.testFramework.fixtures.BasePlatformTestCase
import io.mockk.clearAllMocks
import io.mockk.every
import io.mockk.mockkObject
import io.mockk.unmockkAll
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class TokenUtilTest : BasePlatformTestCase() {
  var receivedEvent: MutableList<GitLabSettingsChangeEvent> = mutableListOf()

  @BeforeEach
  public override fun setUp() {
    super.setUp()

    project.messageBus.connect(testRootDisposable).subscribe(
      GitLabSettingsListener.SETTINGS_CHANGED,
      object : GitLabSettingsListener {
        override fun codeStyleSettingsChanged(event: GitLabSettingsChangeEvent) {
          receivedEvent.add(event)
        }
      }
    )

    mockkObject(ProjectContextService.instance)
    every { ProjectContextService.instance.serverServiceWithoutCallbacks.get().isVersionUpgradeRequired } returns false
  }

  @AfterEach
  override fun tearDown() {
    super.tearDown()

    unmockkAll()
    clearAllMocks()
  }

  @Test
  fun sendEnabledEvent() {
    val token = "dummyToken"

    // Action
    TokenUtil.setToken(token)

    val receivedEvent = receivedEvent.last()
    assertEquals(Status.ENABLED, receivedEvent.status)
    assertEquals(token, TokenUtil.getToken())
  }

  @Test
  fun sendDisabledEvent() {
    val token = ""

    // Action
    TokenUtil.setToken(token)

    val receivedEvent = receivedEvent.last()
    assertEquals(Status.DISABLED, receivedEvent.status)
    assertEquals(token, TokenUtil.getToken())
  }
}
