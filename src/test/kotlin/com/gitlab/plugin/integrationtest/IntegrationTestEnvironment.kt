package com.gitlab.plugin.integrationtest

import io.kotest.core.annotation.EnabledCondition
import io.kotest.core.spec.Spec
import kotlin.reflect.KClass
import kotlin.test.assertNotNull

class IntegrationTestEnvironment(
  private val personalAccessToken: String? = System.getenv("TEST_ACCESS_TOKEN"),
  val gitlabHost: String = System.getenv("TEST_GITLAB_HOST") ?: "https://gitlab.com",
) {
  val personalAccessTokenProvider = {
    assertNotNull(personalAccessToken, "personalAccessToken must not be null")
  }

  companion object {
    private val instances: MutableMap<KClass<out Spec>, IntegrationTestEnvironment> = mutableMapOf()

    fun forSpec(kclass: KClass<out Spec>) = instances.getOrPut(kclass) {
      IntegrationTestEnvironment()
    }
  }

  class Configured : EnabledCondition {
    override fun enabled(
      kclass: KClass<out Spec>
    ) = forSpec(kclass).let {
      it.gitlabHost.isNotEmpty() && it.personalAccessToken.orEmpty().isNotEmpty()
    }
  }
}
