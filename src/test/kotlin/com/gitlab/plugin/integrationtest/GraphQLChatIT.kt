package com.gitlab.plugin.integrationtest

import app.cash.turbine.Event
import app.cash.turbine.test
import com.apollographql.apollo3.api.ApolloResponse
import com.apollographql.apollo3.api.Optional
import com.gitlab.plugin.api.duo.GraphQLApi
import com.gitlab.plugin.api.graphql.ApolloClientFactory
import com.gitlab.plugin.graphql.ChatSubscription
import com.gitlab.plugin.graphql.CurrentUserQuery
import com.gitlab.plugin.graphql.scalars.AiModelID
import com.gitlab.plugin.graphql.scalars.UserID
import com.gitlab.plugin.graphql.type.AiChatInput
import com.intellij.util.net.HttpConfigurable
import io.kotest.common.runBlocking
import io.kotest.core.annotation.EnabledIf
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.collections.shouldNotBeEmpty
import io.kotest.matchers.nulls.shouldBeNull
import io.kotest.matchers.nulls.shouldNotBeNull
import io.kotest.matchers.should
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkStatic
import kotlinx.coroutines.delay
import java.util.UUID
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.time.Duration.Companion.seconds

@EnabledIf(IntegrationTestEnvironment.Configured::class)
class GraphQLChatIT : DescribeSpec({
  val env = IntegrationTestEnvironment.forSpec(GraphQLChatIT::class)
  val apolloClientFactory = ApolloClientFactory(
    gitlabHost = env.gitlabHost,
    tokenProvider = env.personalAccessTokenProvider
  )
  lateinit var currentUser: CurrentUserQuery.CurrentUser
  lateinit var graphqlApi: GraphQLApi

  beforeEach {
    mockkStatic(HttpConfigurable::class)
    every { HttpConfigurable.getInstance() } returns mockk<HttpConfigurable>()

    graphqlApi = GraphQLApi(apolloClientFactory)
    currentUser = runBlocking { assertNotNull(graphqlApi.getCurrentUser()) }
  }

  describe("aiCompletionResponses") {
    it("should receive items") {
      val sid = UUID.randomUUID().toString()
      val sendChatResponse = runBlocking {
        graphqlApi.Chat.send(
          chatInput = AiChatInput(
            content = "Hi Duo from local testing. What can I ask?",
            resourceId = Optional.present(AiModelID(currentUser.id)),
          ),
          clientSubscriptionId = sid,
        )
      }
      val subscriptionFlow = graphqlApi.Chat.subscription(sid, UserID(currentUser.id))
      assertNotNull(sendChatResponse)
      assertEquals(emptyList(), sendChatResponse.errors)

      subscriptionFlow.test(60.seconds) {
        delay(30.seconds)

        val aiCompletionResponses = mutableListOf<ChatSubscription.AiCompletionResponse>()
        val events = cancelAndConsumeRemainingEvents()
        events.forEach {
          if (it is Event.Item<ApolloResponse<ChatSubscription.Data>>) {
            it.value.dataAssertNoErrors.aiCompletionResponse?.let { response ->
              aiCompletionResponses.add(response)
            }
          }
        }
        events.shouldNotBeEmpty()
        aiCompletionResponses.last().should {
          it.shouldNotBeNull()
          it.chunkId.shouldBeNull()
        }
      }
    }
  }
})
