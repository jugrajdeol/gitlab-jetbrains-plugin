package com.gitlab.plugin.codesuggestions.telemetry

import com.gitlab.plugin.services.ProjectContextService
import com.intellij.codeInsight.inline.completion.InlineCompletionEventType
import com.intellij.codeInsight.inline.completion.logs.InlineCompletionUsageTracker
import io.kotest.core.spec.style.DescribeSpec
import io.mockk.*

class InlineCompletionEventListenerTest : DescribeSpec({
  val telemetry: Telemetry = mockk()
  val listener = InlineCompletionEventListener()

  mockkObject(ProjectContextService)

  beforeEach {
    every { ProjectContextService.instance.telemetry } returns telemetry
  }

  afterEach { clearAllMocks() }
  afterSpec { unmockkAll() }

  describe("onShow") {
    val event: InlineCompletionEventType.Show = mockk()

    beforeEach {
      every { telemetry.shown(telemetry.currentContext) } just runs
    }

    it("sends a shown telemetry event with the current context") {
      listener.onShow(event)

      verify(exactly = 1) {
        telemetry.shown(telemetry.currentContext)
      }
    }
  }

  describe("onHide") {
    val event: InlineCompletionEventType.Hide = mockk()

    beforeEach {
      every { telemetry.rejected(telemetry.currentContext) } just runs
    }

    context("when suggestion was displayed") {
      beforeEach {
        every { event.isCurrentlyDisplaying } returns true
      }

      context("and suggestion was not accepted") {
        beforeEach {
          every { event.finishType } returns InlineCompletionUsageTracker.ShownEvents.FinishType.ESCAPE_PRESSED
        }

        it("sends a rejected telemetry event with the current context") {
          listener.onHide(event)

          verify(exactly = 1) {
            telemetry.rejected(telemetry.currentContext)
          }
        }
      }

      context("and suggestion was accepted") {
        beforeEach {
          every { event.finishType } returns InlineCompletionUsageTracker.ShownEvents.FinishType.SELECTED
        }

        it("does not send a rejected telemetry event") {
          listener.onHide(event)

          verify(exactly = 0) {
            telemetry.rejected(telemetry.currentContext)
          }
        }
      }
    }

    context("when suggestion was not displayed") {
      beforeEach {
        every { event.isCurrentlyDisplaying } returns false
      }

      it("does not send a rejected telemetry event") {
        listener.onHide(event)

        verify(exactly = 0) {
          telemetry.rejected(telemetry.currentContext)
        }
      }
    }
  }

  describe("onInsert") {
    val event: InlineCompletionEventType.Insert = mockk()

    beforeEach {
      every { telemetry.accepted(telemetry.currentContext) } just runs
    }

    it("sends an accepted telemetry event with the current context") {
      listener.onInsert(event)

      verify(exactly = 1) {
        telemetry.accepted(telemetry.currentContext)
      }
    }
  }
})
