package com.gitlab.plugin.codesuggestions.telemetry

import com.intellij.codeInsight.inline.completion.InlineCompletion
import com.intellij.codeInsight.inline.completion.InlineCompletionHandler
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.editor.event.EditorFactoryEvent
import io.kotest.assertions.throwables.shouldNotThrowAny
import io.kotest.core.spec.style.DescribeSpec
import io.mockk.*

class EditorListenerTest : DescribeSpec({
  val editorListener = EditorListener()
  val event: EditorFactoryEvent = mockk()
  val editor: Editor = mockk()

  mockkObject(InlineCompletion)

  beforeEach {
    every { event.editor } returns editor
  }

  afterEach { clearAllMocks() }
  afterSpec { unmockkAll() }

  describe("editorCreated") {
    context("when an InlineCompletionHandler exists for the editor") {
      val inlineCompletionHandler: InlineCompletionHandler = mockk()

      beforeEach {
        every { InlineCompletion.getHandlerOrNull(editor) } returns inlineCompletionHandler
        every { inlineCompletionHandler.addEventListener(any<InlineCompletionEventListener>()) } just runs
      }

      it("registers the InlineCompletionEventListener") {
        editorListener.editorCreated(event)

        verify(exactly = 1) {
          inlineCompletionHandler.addEventListener(any<InlineCompletionEventListener>())
        }
      }
    }

    context("when no InlineCompletionHandler exists for the editor") {
      beforeEach {
        every { InlineCompletion.getHandlerOrNull(editor) } returns null
      }

      it("does nothing") {
        shouldNotThrowAny {
          editorListener.editorCreated(event)
        }
      }
    }
  }
})
